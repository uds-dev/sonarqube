resource "aws_security_group" "web" {
  description = "controls access to the application ELB"

  vpc_id = var.vpc_id
  name   = "sonarqube-lb"

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }
}

resource "aws_security_group" "sonarqube" {
  description = "controls direct access to application instances"
  vpc_id      = var.vpc_id
  name        = "sonarqube-task-sg"

  ingress {
    protocol  = "tcp"
    from_port = var.sonarqube_port
    to_port   = var.sonarqube_port

    security_groups = [
      "${aws_security_group.web.id}",
    ]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }
}

resource "aws_security_group" "db" {
  name        = "sonarqube-db-sg"
  description = "Allow all inbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    from_port = 5432
    to_port   = 5432
    protocol  = "TCP"

    security_groups = [
      "${aws_security_group.sonarqube.id}",
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
