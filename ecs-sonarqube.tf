data "template_file" "sonarqube_task_definition" {
  template = file("${path.module}/task-definitions/sonarqube.json.tpl")

  vars = {
    sonar_jdbc_url         = "jdbc:postgresql://${aws_db_instance.sonarqube.endpoint}/${var.db_name}"
    sonar_jdbc_username    = var.db_username
    sonar_jdbc_password    = random_string.db_password.result
    sonarqube_version      = var.sonarqube_version
    container_cpu          = var.sonarqube_container_cpu
    container_memory       = var.sonarqube_container_memory
    sonarqube_port         = var.sonarqube_port
    sonar_search_ops       = var.sonar_search_ops
    log_group_region       = var.aws_region
    log_group_sonarqube    = aws_cloudwatch_log_group.sonarqube.name
  }
}

resource "aws_ecs_task_definition" "sonarqube" {
  family                   = "sonarqube"
  container_definitions    = data.template_file.sonarqube_task_definition.rendered
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"

  task_role_arn      = aws_iam_role.ecs_task.arn
  execution_role_arn = aws_iam_role.ecs_task.arn

  cpu    = var.sonarqube_task_cpu
  memory = var.sonarqube_task_memory
}

resource "aws_ecs_service" "sonarqube" {
  name            = "sonarqube"
  cluster         = var.aws_ecs_cluster_id
  task_definition = aws_ecs_task_definition.sonarqube.arn
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = ["${aws_security_group.sonarqube.id}"]
    subnets          = var.vpc_subnets_public_ids
    assign_public_ip = true
  }
  load_balancer {
    target_group_arn = aws_alb_target_group.sonarqube.id
    container_name   = "sonarqube"
    container_port   = var.sonarqube_port
  }
  depends_on = [
    aws_alb_listener.front_end_80,
    aws_alb_listener.front_end_443,
  ]
}
