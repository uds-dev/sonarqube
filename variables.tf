variable "vpc_subnets_private_ids" {
  type = list
}

variable "vpc_subnets_public_ids" {
  type = list
}

variable "vpc_id" {
  type = string
}

variable "aws_ecs_cluster_id" {
  type = string
}
variable "aws_region" {
  description = "aws region."
  default     = "eu-west-1"
}
variable "identifier" {
  default     = "sonarqube-rds"
  description = "Identifier for your DB"
}

variable "storage" {
  default     = "100"
  description = "Storage size in GB"
}

variable "engine" {
  default     = "postgres"
  description = "Engine type, example values mysql, postgres"
}

variable "engine_version" {
  description = "Engine version"
  default = {
    postgres = "12.4"
  }
}
variable "instance_class" {
  default     = "db.t3.medium"
  description = "Instance class"
}

variable "db_name" {
  default     = "sonarqube"
  description = "db name"
}

variable "db_username" {
  default     = "sonar"
  description = "database user name"
}

variable "alb_port" {
  description = "ALB frond end port"
  default     = "80"
}

variable "sonarqube_version" {
  description = "sonarqube server version."
}

variable "sonar_search_ops" {
  description = "sonarqube elasticsearch ops."
  default     = "-Dnode.store.allow_mmapfs=false"
}
variable "sonarqube_task_cpu" {
  description = "cpu usage of ecs task"
  default     = 512
}

variable "sonarqube_task_memory" {
  description = "memory usage of ecs task"
  default     = 2048
}

variable "sonarqube_container_cpu" {
  description = "cpu usage of ecs container"
  default     = 512
}
variable "sonarqube_container_memory" {
  description = "memory usage of ecs container"
  default     = 2048
}

variable "sonarqube_port" {
  description = "sonarqube port."
  default     = "9000"
}