resource "aws_db_subnet_group" "db" {
  name       = "sonarqube"
  subnet_ids = var.vpc_subnets_private_ids

  tags = {
    Name = "Sonarqube DB subnet group"
  }
}

resource "random_string" "db_password" {
  special = false
  length  = 20
}

resource "aws_db_instance" "sonarqube" {
  depends_on = [
    aws_security_group.db,
  ]
  identifier                = var.identifier
  allocated_storage         = var.storage
  engine                    = var.engine
  engine_version            = lookup(var.engine_version, var.engine)
  instance_class            = var.instance_class
  name                      = var.db_name
  username                  = var.db_username
  password                  = random_string.db_password.result
  vpc_security_group_ids    = ["${aws_security_group.db.id}"]
  db_subnet_group_name      = "main"
  skip_final_snapshot       = true
  final_snapshot_identifier = "sonarqube-${md5(timestamp())}"
}
