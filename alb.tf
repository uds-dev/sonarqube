resource "aws_alb_target_group" "sonarqube" {
  name        = "sonarqube-ecs"
  port        = 3000
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = "ip"

  health_check {
    path                = "/healthz"
    matcher             = "200"
    timeout             = "5"
    healthy_threshold   = "3"
    unhealthy_threshold = "2"
  }
}

resource "aws_alb" "front" {
  name            = "sonarqube-front-alb"
  internal        = false
  security_groups = ["${aws_security_group.web.id}"]
  subnets         = var.vpc_subnets_public_ids

  enable_deletion_protection = false

  tags = {
    Name        = "sonarqube"
  }
}

resource "aws_alb_listener" "front_end_80" {
  load_balancer_arn = aws_alb.front.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type          = "redirect"
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_alb_listener" "front_end_443" {
  load_balancer_arn = aws_alb.front.arn
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = "arn:aws:acm:eu-west-1:058382256360:certificate/5da1321c-8a72-47b6-9eab-0fbee1137700"
  ssl_policy        = "ELBSecurityPolicy-2016-08"

  default_action {
    target_group_arn = aws_alb_target_group.sonarqube.arn
    type             = "forward"
  }
}
