[
  {
    "name": "sonarqube",
    "image": "sonarqube:${sonarqube_version}",
    "cpu": ${container_cpu},
    "memory": ${container_memory},
    "portMappings": [{
      "containerPort": ${sonarqube_port},
      "protocol": "tcp"
    }],
    "environment": [
      { "name": "SONAR_JDBC_URL", "value": "${sonar_jdbc_url}" },
      { "name": "SONAR_JDBC_USERNAME", "value": "${sonar_jdbc_username}" },
      { "name": "SONAR_JDBC_PASSWORD", "value": "${sonar_jdbc_password}" },
      { "name": "SONAR_SEARCH_JAVAADDITIONALOPTS", "value": "${sonar_search_ops}" }
    ],
    "ulimits": [
        {
          "softLimit": 65535,
          "hardLimit": 65535,
          "name": "nofile"
        }
    ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-stream-prefix": "sonarqube",
        "awslogs-group": "${log_group_sonarqube}",
        "awslogs-region": "${log_group_region}"
      }
    }
  }
]